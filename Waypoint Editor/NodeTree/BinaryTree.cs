﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waypoint_Editor
{
    public class BinaryTree
    {
        public bool IsEmpty()
        {
            return (m_pRoot == null);
        }

        public TreeNode Find(int a_nValue)
        {
            if (FindNode(a_nValue, out TreeNode c_Node, out TreeNode p_Node))
            {
                return c_Node;
            }
            return null;
        }
      
        private bool FindNode(int a_nSearchValue, out TreeNode ppOutNode, out TreeNode ppOutParent)
        {
            TreeNode c_Node = m_pRoot;
            TreeNode ParentNode = null;

            while (c_Node != null)
            {
                if (a_nSearchValue == c_Node.Value)
                {
                    ppOutNode = c_Node;
                    ppOutParent = ParentNode;
                    return true;
                }
                else
                {
                    ParentNode = c_Node;
                    if (a_nSearchValue < c_Node.Value)
                    {
                        c_Node = c_Node.Left;
                    }
                    else
                    {
                        c_Node = c_Node.Right;
                    }
                }
            }
            ppOutParent = ppOutNode = null;
            return false;
        }

        public void Insert(int a_nValue)
        {
            TreeNode Node = new TreeNode(a_nValue);
            TreeNode c_Node = m_pRoot;
            TreeNode ParentNode = null;

            if (IsEmpty())
            {
                m_pRoot = Node;
            }
            else
            {
                while (c_Node != null)
                {
                    if (a_nValue < c_Node.Value)
                    {
                        ParentNode = c_Node;
                        c_Node = c_Node.Left;
                    }
                    else if (a_nValue > c_Node.Value)
                    {
                        ParentNode = c_Node;
                        c_Node = c_Node.Right;
                    }
                    else if (a_nValue == c_Node.Value)
                    {
                        return;
                    }
                }
                if (a_nValue < ParentNode.Value)
                {
                    ParentNode.Left = Node;
                }
                else
                {
                    ParentNode.Right = Node;
                }
            }
        }

        public void Remove(int a_nValue)
        {
            TreeNode c_Node;
            TreeNode ParentNode;

            if (!FindNode(a_nValue, out c_Node, out ParentNode))
                return;

            if (c_Node != null && c_Node.HasRight())
            {
                TreeNode minNode = c_Node.Right;
                TreeNode minNodeParent = c_Node;
                while (minNode.HasLeft())
                {
                    minNodeParent = minNode;
                    minNode = minNode.Left;
                }

                if (c_Node != m_pRoot)
                {
                    c_Node.Value = minNode.Value;
                }
                else
                {
                    m_pRoot.Value = minNode.Value;
                }

                // Clear to here
                if (minNodeParent.Left == minNode)
                {
                    minNodeParent.Left = minNode.Right;
                }
                else if (minNode.Right != null)
                {
                    minNodeParent.Right = minNode.Right;
                }
                else if (minNode == null)
                {
                    minNodeParent.Right = null;
                }
                minNode = null;
            }

            else
            {
                if (c_Node == m_pRoot && !m_pRoot.HasRight() && !m_pRoot.HasLeft())
                {
                    m_pRoot = null;
                    return;
                }
                if (c_Node == m_pRoot)
                {
                    TreeNode minNode = c_Node.Left;
                    TreeNode minNodeParent = c_Node;

                    while (minNode.HasRight())
                    {
                        minNodeParent = minNode;
                        minNode = minNode.Right;
                    }

                    m_pRoot.Value = minNode.Value;

                    if (minNodeParent.Left == minNode)
                    {
                        minNodeParent.Left = minNode.Left;
                    }
                    else
                    {
                        minNodeParent.Right = minNode.Left;
                    }
                    minNode = null;
                }

                if (ParentNode != null)
                {
                    if (ParentNode.Left == c_Node)
                    {
                        ParentNode.Left = c_Node.Left;
                    }
                    else
                    {
                        ParentNode.Right = c_Node.Left;
                    }
                    if (ParentNode == m_pRoot)
                    {
                        c_Node.Left = m_pRoot;
                    }
                }
                if (c_Node != m_pRoot)
                    c_Node = null;
            }
        }

        public TreeNode m_pRoot = null;
    }
}
