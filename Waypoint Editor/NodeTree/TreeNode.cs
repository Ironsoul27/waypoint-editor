﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;

namespace Waypoint_Editor
{
    public class TreeNode
    {
        public TreeNode(int value)
        {
            this.Value = value;
        }

        public bool HasLeft() { return (Left != null); }
        public bool HasRight() { return (Right != null); }
        public bool IsValid(TreeNode Node) { return (Node == null); }

        public int Value { get; set; }

        private TreeNode left = null;
        private TreeNode right = null;

        public TreeNode Left
        {
            get
            {
                return left;
            }
            set
            {
                if (value != null)
                    left = value;
                else
                    Debug.Assert(value != null);
            }
        }

        public TreeNode Right
        {
            get
            {
                return right;
            }
            set
            {
                if (value != null)
                    right = value;
                else
                    Debug.Assert(value != null);
            }
        }

        private int Radius { get; set; } = 30;

        public void Draw(ref Graphics g, int x, int y, bool selected)
        {
            Pen nodeColour = new Pen(Color.Gray);
            Font font = new Font("Arial", 10);
            SolidBrush textBrush = new SolidBrush(Color.Black);
            SolidBrush nodeBrush = new SolidBrush(Color.PaleVioletRed);
            float XPosition = x - (Radius / 2);
            float YPosition = y - (Radius / 2);


            g.DrawEllipse(nodeColour, XPosition, YPosition, Radius, Radius);
            g.FillEllipse(nodeBrush, XPosition, YPosition, Radius, Radius);

            if (selected == true)
            {
                SolidBrush selectedBrush = new SolidBrush(Color.DarkGoldenrod);
                g.DrawEllipse(nodeColour, XPosition, YPosition, Radius - 3, Radius - 3);
                g.FillEllipse(selectedBrush, XPosition, YPosition, Radius - 3, Radius - 3);
            }
           

            g.DrawString(Value.ToString(), font, textBrush, XPosition, YPosition);
        }

    }
}
