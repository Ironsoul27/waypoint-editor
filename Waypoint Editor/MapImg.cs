﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.Serialization;

namespace Waypoint_Editor
{
    [Serializable]
    public class MapImg : ISerializable
    {
        public Image image = null;

        public MapImg()
        {}

        public MapImg(string path)
        {
            Load(path);
        }

        public MapImg(SerializationInfo info, StreamingContext context)
        {
            image = (Image)info.GetValue("image", typeof(Image));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("image", image, typeof(Image));
        }

        public int GetWidth => (image != null) ? image.Width : 0;

        public int GetHeight => (image != null) ? image.Height : 0;

        public void Load(string path)
        {
            image = Image.FromFile(path);
        }
    }
}
