﻿namespace Waypoint_Editor
{
    partial class BinaryTreeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Canvas = new System.Windows.Forms.PictureBox();
            this.AddNodeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RemoveNodeTextBox = new System.Windows.Forms.TextBox();
            this.ClearTreeButton = new System.Windows.Forms.Button();
            this.FindNodeTextBox = new System.Windows.Forms.TextBox();
            this.AddNodeButton = new System.Windows.Forms.Button();
            this.RemoveNodeButton = new System.Windows.Forms.Button();
            this.FindNodeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).BeginInit();
            this.SuspendLayout();
            // 
            // Canvas
            // 
            this.Canvas.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.Canvas.Location = new System.Drawing.Point(12, 12);
            this.Canvas.Name = "Canvas";
            this.Canvas.Size = new System.Drawing.Size(776, 303);
            this.Canvas.TabIndex = 0;
            this.Canvas.TabStop = false;
            // 
            // AddNodeTextBox
            // 
            this.AddNodeTextBox.Location = new System.Drawing.Point(12, 343);
            this.AddNodeTextBox.Name = "AddNodeTextBox";
            this.AddNodeTextBox.Size = new System.Drawing.Size(132, 20);
            this.AddNodeTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 327);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Node Values";
            // 
            // RemoveNodeTextBox
            // 
            this.RemoveNodeTextBox.Location = new System.Drawing.Point(12, 379);
            this.RemoveNodeTextBox.Name = "RemoveNodeTextBox";
            this.RemoveNodeTextBox.Size = new System.Drawing.Size(132, 20);
            this.RemoveNodeTextBox.TabIndex = 3;
            // 
            // ClearTreeButton
            // 
            this.ClearTreeButton.Location = new System.Drawing.Point(689, 322);
            this.ClearTreeButton.Name = "ClearTreeButton";
            this.ClearTreeButton.Size = new System.Drawing.Size(99, 23);
            this.ClearTreeButton.TabIndex = 5;
            this.ClearTreeButton.Text = "Clear NodeTree";
            this.ClearTreeButton.UseVisualStyleBackColor = true;
            // 
            // FindNodeTextBox
            // 
            this.FindNodeTextBox.Location = new System.Drawing.Point(12, 418);
            this.FindNodeTextBox.Name = "FindNodeTextBox";
            this.FindNodeTextBox.Size = new System.Drawing.Size(132, 20);
            this.FindNodeTextBox.TabIndex = 6;
            // 
            // AddNodeButton
            // 
            this.AddNodeButton.Location = new System.Drawing.Point(150, 341);
            this.AddNodeButton.Name = "AddNodeButton";
            this.AddNodeButton.Size = new System.Drawing.Size(75, 23);
            this.AddNodeButton.TabIndex = 8;
            this.AddNodeButton.Text = "Add Node";
            this.AddNodeButton.UseVisualStyleBackColor = true;
            this.AddNodeButton.Click += new System.EventHandler(this.AddNodeButton_Click);
            // 
            // RemoveNodeButton
            // 
            this.RemoveNodeButton.Location = new System.Drawing.Point(150, 377);
            this.RemoveNodeButton.Name = "RemoveNodeButton";
            this.RemoveNodeButton.Size = new System.Drawing.Size(86, 23);
            this.RemoveNodeButton.TabIndex = 9;
            this.RemoveNodeButton.Text = "Remove Node";
            this.RemoveNodeButton.UseVisualStyleBackColor = true;
            this.RemoveNodeButton.Click += new System.EventHandler(this.RemoveNodeButton_Click);
            // 
            // FindNodeButton
            // 
            this.FindNodeButton.Location = new System.Drawing.Point(150, 416);
            this.FindNodeButton.Name = "FindNodeButton";
            this.FindNodeButton.Size = new System.Drawing.Size(75, 23);
            this.FindNodeButton.TabIndex = 10;
            this.FindNodeButton.Text = "Find Node";
            this.FindNodeButton.UseVisualStyleBackColor = true;
            this.FindNodeButton.Click += new System.EventHandler(this.FindNodeButton_Click);
            // 
            // BinaryTreeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.FindNodeButton);
            this.Controls.Add(this.RemoveNodeButton);
            this.Controls.Add(this.AddNodeButton);
            this.Controls.Add(this.FindNodeTextBox);
            this.Controls.Add(this.ClearTreeButton);
            this.Controls.Add(this.RemoveNodeTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddNodeTextBox);
            this.Controls.Add(this.Canvas);
            this.Name = "BinaryTreeForm";
            this.Text = "BinaryTreeForm";
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Canvas;
        private System.Windows.Forms.TextBox AddNodeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RemoveNodeTextBox;
        private System.Windows.Forms.Button ClearTreeButton;
        private System.Windows.Forms.TextBox FindNodeTextBox;
        private System.Windows.Forms.Button AddNodeButton;
        private System.Windows.Forms.Button RemoveNodeButton;
        private System.Windows.Forms.Button FindNodeButton;
    }
}