﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;

using System.Numerics;

namespace Waypoint_Editor
{
    [Serializable]
    public partial class Form1 : Form
    {
        public MapImg Map { get; private set; } = new MapImg();
        Bitmap drawArea;
        Hashtable Nodes;
        List<Node> selectedNodes;        
        // Selected node
        Node activeNode = null;
        Rectangle selectionBox = new Rectangle();
        uint numberOfNodes = 1;
        float nodeRadius = 25;
        bool mouseDown = false;
        Vector2 selectBoxLockPoint = new Vector2();
        Vector2 selectFollowOffset = new Vector2();
        Vector2 oldMousePosition = new Vector2();

        public enum ModesList { AddNode, RemoveNode, MoveNode, ConnectNode, SelectNode };

        ModesList Mode;        

        public Form1()
        {
            InitializeComponent();
            drawArea = new Bitmap(Canvas.Width, Canvas.Height);            
            Nodes = new Hashtable();
            Canvas.SizeMode = PictureBoxSizeMode.Zoom;
            AutoScaleMode = AutoScaleMode.Dpi;
            selectedNodes = new List<Node>();
        }

        public void SerializeItem(string filename, IFormatter formatter)
        {
            FileStream s = new FileStream(filename, FileMode.Create);

            formatter.Serialize(s, Nodes);
            formatter.Serialize(s, Map);
            s.Close();
        }

        public void DeserializeItem(string filename, IFormatter formatter)
        {
            FileStream s = new FileStream(filename, FileMode.Open);
            Nodes = (Hashtable)formatter.Deserialize(s);
            Map = (MapImg)formatter.Deserialize(s);
            numberOfNodes = ((uint)Nodes.Count + 1);
            s.Close();
            Draw();
        }

        private void Draw()
        {
            PerformAutoScale();

            Canvas.DrawToBitmap(drawArea, Canvas.Bounds);

            Graphics g;
            g = Graphics.FromImage(drawArea);
            g.Clear(Color.White);

            if (Map.image != null)
                g.DrawImage(Map.image, 0, 0);

            Pen BluePen = new Pen(Brushes.Blue);
            Pen BlackPen = new Pen(Brushes.Black);
            SolidBrush nodeBrush = new SolidBrush(Color.Blue);
            SolidBrush textBrush = new SolidBrush(Color.Black);
            Font font = new Font("Arial", 10);

            if (Nodes != null)
            {
                foreach (DictionaryEntry n in Nodes)
                {
                    Node node = (Node)n.Value;
                    Vector2 position = node.Position;
                    float radius = node.Radius;

                    // Draw nodes
                    g.DrawEllipse(BluePen, position.X - (radius / 2), position.Y - (radius / 2), radius, radius);
                    g.FillEllipse(nodeBrush, position.X - (radius / 2), position.Y - (radius / 2), radius, radius);

                    // Draw connections
                    foreach(DictionaryEntry connection in node.Connections)
                    {
                        Node c = (Node)connection.Value;
                        g.DrawLine(BluePen, node.Position.X, node.Position.Y, c.Position.X, c.Position.Y);
                    }

                    //g.DrawString(node.ID, font, textBrush, node.Position.X, node.Position.Y);

                    //Rectangle selectcheck = node.SelectionCheck;
                    //g.DrawRectangle(BlackPen, selectcheck.X - (radius / 2), selectcheck.Y - (radius / 2), selectcheck.Width, selectcheck.Height);

                }

                // Draw selectedNode highlights
                foreach (Node n in selectedNodes)
                {
                    float highlightCircleRadius = 12;

                    g.DrawEllipse(BlackPen, n.Position.X - (n.Radius / 2) - (highlightCircleRadius / 2),
                       n.Position.Y - (n.Radius / 2) - (highlightCircleRadius / 2),
                       n.Radius + highlightCircleRadius,
                       n.Radius + highlightCircleRadius);
                }

                switch (Mode)
                {
                    // Draw Select Box
                    case ModesList.SelectNode:
                        {                     
                            if (selectionBox.Height > 0 || selectionBox.Width > 0)
                            {
                                g.DrawRectangle(BlackPen, selectionBox);
                            }
                            break;
                        }
                    default:
                        break;
                }
            }                    

            g.Dispose();

            Canvas.Image = drawArea;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            drawArea = new Bitmap(Canvas.Width, Canvas.Height);
        }

        private void LoadImageButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                if (dlg.CheckFileExists == true)
                {
                    Map = new MapImg(dlg.FileName);
                    Draw();
                }
            }
        }

        private void AddNodeButton_Click(object sender, EventArgs e)
        {
            Mode = ModesList.AddNode;
            ModeSelectText.Text = "Add Nodes";         
        }

        private void RemoveNodeButton_Click(object sender, EventArgs e)
        {
            Mode = ModesList.RemoveNode;
            ModeSelectText.Text = "Remove Nodes";

            if (selectedNodes.Count() != 0)
            {
                int nodeCount = selectedNodes.Count();

                for (int i = 0; i < nodeCount; i++)
                {
                    // Assign to the first node to ensure access doesn't fall out of lists scope
                    Node node = selectedNodes[0];

                    string id = node.ID;

                    List<Node> tempNodeList = new List<Node>();

                    foreach (DictionaryEntry connection in node.Connections)
                    {
                        Node temp = (Node)connection.Value;
                        tempNodeList.Add(temp);
                    }

                    foreach (Node connectedNode in tempNodeList)
                    {
                        connectedNode.RemoveConnection(ref id);
                    }              

                    node.Connections.Clear();
                    Nodes.Remove(node.ID);
                    selectedNodes.Remove(node);
                }             
            }
            Draw();
        }

        private void MoveNodeButton_Click(object sender, EventArgs e)
        {
            Mode = ModesList.MoveNode;
            ModeSelectText.Text = "Move Nodes";
        }

        private void AddConnectionButton_Click(object sender, EventArgs e)
        {
            Mode = ModesList.ConnectNode;
            ModeSelectText.Text = "Connect Nodes";
        }

        private void SelectNodesButton_Click(object sender, EventArgs e)
        {
            Mode = ModesList.SelectNode;
            ModeSelectText.Text = "Select Nodes";
        }

        private void ClearNodesButton_Click(object sender, EventArgs e)
        {
            Nodes.Clear();
            selectedNodes.Clear();
            numberOfNodes = 1;
            Draw();
        }

        private void Canvas_Click(object sender, EventArgs e)
        {
            MouseEventArgs mouse = e as MouseEventArgs;
            Vector2 mousePosition = new Vector2(mouse.X, mouse.Y);

            switch (Mode)
            {
                case ModesList.AddNode:
                    {                     
                        string nodeId = "Node " + numberOfNodes++.ToString();
                        Node node = new Node(ref nodeId, nodeRadius, ref mousePosition);
                        Nodes[nodeId] = node;

                        break;
                    }
                   
                case ModesList.RemoveNode:
                    {
                        // Loop over nodes and look for overlap
                        foreach (DictionaryEntry n in Nodes)
                        {
                            Node node = (Node)n.Value;

                            if (Overlaps(ref mousePosition, ref node, out Node deleteNode))
                            {
                                if (deleteNode != null)
                                {
                                    // Delete connections to this node 
                                    string id = deleteNode.ID;
                                    List<Node> tempNodeList = new List<Node>();

                                    foreach (DictionaryEntry connection in deleteNode.Connections)
                                    {
                                        Node temp = (Node)connection.Value;
                                        tempNodeList.Add(temp);
                                    }

                                    foreach (Node connectedNode in tempNodeList)
                                    {
                                        connectedNode.RemoveConnection(ref id);
                                    }
                                  
                                    // Fix draw in binary tree editor

                                    deleteNode.Connections.Clear();
                                    Nodes.Remove(deleteNode.ID);
                                    selectedNodes.Remove(deleteNode);
                                    break;
                                }
                            }
                        }
                        break;
                    }               
            }
            Draw();
        }

        private void Canvas_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;

            Vector2 mousePosition = new Vector2(e.X, e.Y);

            switch (Mode)
            {
                case ModesList.SelectNode:                    
                    {
                        if (activeNode == null)
                        {
                            Node selectedNode = null;
                            selectBoxLockPoint = mousePosition;

                            foreach (DictionaryEntry n in Nodes)
                            {
                                Node node = (Node)n.Value;
                                if (Overlaps(ref mousePosition, ref node, out selectedNode))
                                {
                                    if (!selectedNodes.Contains(node))
                                    {
                                        selectedNodes.Add(node);
                                    }                                 
                                }
                            }
                        }                    

                        break;
                    }
                case ModesList.MoveNode:
                    {
                        selectFollowOffset = mousePosition;
                        oldMousePosition = mousePosition;
                        break;
                    }

                default:
                    return;
            }
        }

        private bool Overlaps(ref Vector2 first, ref Node second, out Node node)
        {
            Vector2 dir = new Vector2(first.X - second.Position.X, first.Y - second.Position.Y);
            float magnitude = (dir.X * dir.X + dir.Y * dir.Y);

            if (magnitude < second.Radius * second.Radius)
            {
                node = second;
                return true;
            }
            
            node = null;
                return false;
        }

        public class Pair<T, U>
        {
            public Pair()
            { }      

            public Pair(T first, U second)
            {
                this.First = first;
                this.Second = second;
            }

            public T First { get; set; }
            public U Second { get; set; }

        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            Vector2 mousePosition = new Vector2(e.X, e.Y);
     
            switch (Mode)
            {   // Move selected nodes with mouse             
                case ModesList.MoveNode:
                    {
                        // Clamp mouse anchor to screen space
                        if (mousePosition.X > Canvas.Width)
                            mousePosition.X = Canvas.Width;

                        if (mousePosition.X < 0)
                            mousePosition.X = 0;

                        if (mousePosition.Y > Canvas.Height)
                            mousePosition.Y = Canvas.Height;

                        if (mousePosition.Y < 0)
                            mousePosition.Y = 0;

                        Vector2 newPosition = new Vector2(mousePosition.X - oldMousePosition.X, mousePosition.Y - oldMousePosition.Y);

                        if (selectedNodes.Count() != 0 && mouseDown)
                        {
                            foreach (Node n in selectedNodes)
                            {
                                n.Position = n.Position + newPosition;
                                Rectangle temp = n.SelectionCheck;
                                temp.Location = new Point((int)n.Position.X, (int)n.Position.Y);
                                n.SelectionCheck = temp;                                
                            }
                        }
                        Draw();

                        break;
                    }
                    // Drag selection box
                case ModesList.SelectNode:
                    {
                        if (mouseDown)
                        {
                            selectionBox = Rectangle.FromLTRB((int)selectBoxLockPoint.X, (int)selectBoxLockPoint.Y, (int)mousePosition.X, (int)mousePosition.Y);
                            Draw();

                            foreach (DictionaryEntry n in Nodes)
                            {
                                Node node = (Node)n.Value;
                                Vector2 nodePosition = node.Position;

                                if (selectionBox.IntersectsWith(node.SelectionCheck))
                                {
                                    if (!selectedNodes.Contains(node))
                                    {
                                        selectedNodes.Add(node);
                                    }
                                }                             
                            }                             
                        }
                        break;
                    }
            }
            oldMousePosition = mousePosition;
        }

        private void Canvas_MouseUp(object sender, MouseEventArgs e)
        {
            if (activeNode != null)
                activeNode = null;
            mouseDown = false;
            selectionBox.Size = new Size(0, 0);
            Draw();
        }             

        private void SaveProjectButton_Click(object sender, EventArgs e)
        {
            string filename = "data.myData";
            IFormatter formatter = new SoapFormatter();
            SerializeItem(filename, formatter);
        }

        private void LoadProjectButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "All Files (*.*)|*.*";
            //Text Files(*.txt)| *.txt |
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {            
                string filename = openFileDialog.FileName;

                IFormatter formatter = new SoapFormatter();
                DeserializeItem(filename, formatter);
            }
            Draw();
        }

        private void NodeSizeTextBox_TextChanged(object sender, EventArgs e)
        {
            float nodeSize;
            if (float.TryParse(NodeSizeTextBox.Text, out nodeSize) == true)
            {
                nodeRadius = nodeSize;
            }
            NodeSizeTextBox.Text = nodeRadius.ToString();
        }

        private void UnselectNodesButton_Click(object sender, EventArgs e)
        {
            selectedNodes.Clear();
            Draw();
        }

        private void ConnectAllNodesButton_Click(object sender, EventArgs e)
        {
            foreach(Node n in selectedNodes)
            {
                foreach(Node m in selectedNodes)
                {
                    string id = m.ID;    
                    if (!n.IsConnected(ref id))
                    {
                        Node temp = m;
                        string node_id = m.ID;
                        n.AddConnection(ref node_id, ref temp);
                    }
                }                
            }
            Draw();
        }

        private void RemoveConnectionsButton_Click(object sender, EventArgs e)
        {
            foreach (Node node in selectedNodes)
            {
                node.Connections.Clear();           
            }
            Draw();
        }
    }
}