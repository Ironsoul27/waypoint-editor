﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Waypoint_Editor
{
    public partial class BinaryTreeForm : Form
    {
        Bitmap drawArea;
        BinaryTree binaryTree;
        TreeNode selectedNode = null;
        int rootSpawnPositionHeight = 7;
        int horizontalSpacing = 150;
        int verticalSpacing = 70;

        public BinaryTreeForm()
        {
            InitializeComponent();
            drawArea = new Bitmap(Canvas.Width, Canvas.Height);
            binaryTree = new BinaryTree();

            Canvas.Image = drawArea;
        }

        private void Draw(ref TreeNode pNode, int x, int y, float horizontalSpacing, ref TreeNode selected)
        {
            //Canvas.DrawToBitmap(drawArea, Canvas.Bounds);

            Graphics g;
            g = Graphics.FromImage(drawArea);
            g.Clear(Color.White);

            Pen NodeConnectionsColour = new Pen(Color.Black);

            if (selected == null)
                selected = binaryTree.m_pRoot;

            DrawBinaryTree(ref pNode, x, y, horizontalSpacing, ref selected, ref NodeConnectionsColour, ref g);

            // Loop over tree and draw Nodes
            //while(true)
            //{
            //    horizontalSpacing /= 2;

            //    if (pNode != null)
            //    {
            //        if (pNode.HasLeft())
            //        {
            //            g.DrawLine(NodeConnectionsColour, x, y, x - horizontalSpacing, y - verticalSpacing);
            //            pNode = pNode.Left;
            //            y -= verticalSpacing;
            //            continue;
            //        }

            //        if (pNode.HasRight())
            //        {
            //            g.DrawLine(NodeConnectionsColour, x, y, x - horizontalSpacing, y - verticalSpacing);
            //            pNode = pNode.Right;
            //            y -= verticalSpacing;
            //            continue;
            //        }

            //        if (selected == null)
            //            selected = binaryTree.m_pRoot;

            //        // Draw Node
            //        pNode.Draw(ref g, x, y, (selected == pNode));

            //        // if (!pNode.HasLeft() && !pNode.HasRight())
            //           // break;
            //    }
            //}

            g.Dispose();

            Canvas.Invalidate();
        }

        private void DrawBinaryTree(ref TreeNode pNode, int x, int y, float horizontalSpacing, ref TreeNode selected, ref Pen nodeConnectionsColour, ref Graphics g)
        {
            horizontalSpacing /= 2;

            if (pNode != null)
            {
                if (pNode.HasLeft())
                {
                    g.DrawLine(nodeConnectionsColour, x, y, x - horizontalSpacing, y + verticalSpacing);
                    TreeNode leftNode = pNode.Left;
                    DrawBinaryTree(ref leftNode, x - (int)horizontalSpacing, y + verticalSpacing, horizontalSpacing, ref selected, ref nodeConnectionsColour, ref g);
                }

                if (pNode.HasRight())
                {
                    g.DrawLine(nodeConnectionsColour, x, y, x + horizontalSpacing, y + verticalSpacing);
                    TreeNode rightNode = pNode.Right;
                    DrawBinaryTree(ref rightNode, x + (int)horizontalSpacing, y + verticalSpacing, horizontalSpacing, ref selected, ref nodeConnectionsColour, ref g);
                }

                if (selected == null)
                    selected = binaryTree.m_pRoot;

                // Draw Node
                pNode.Draw(ref g, x, y, (selected == pNode));
            }
        }

        private void AddNodeButton_Click(object sender, EventArgs e)
        {
            int value;
            if (int.TryParse(AddNodeTextBox.Text, out value) == true)
            {
                binaryTree.Insert(value);
            }
            else
            {
                return;
            }

            Draw(ref binaryTree.m_pRoot, Canvas.Location.X + (Canvas.Width / 2), Canvas.Location.Y + (Canvas.Height / rootSpawnPositionHeight), horizontalSpacing, ref selectedNode);
        }

        private void FindNodeButton_Click(object sender, EventArgs e)
        {
            int value;
            if (int.TryParse(FindNodeTextBox.Text, out value) == true)
            {
               selectedNode = binaryTree.Find(value);
            }
            else
            {
                return;
            }

            Draw(ref binaryTree.m_pRoot, Canvas.Location.X + (Canvas.Width / 2), Canvas.Location.Y + (Canvas.Height / rootSpawnPositionHeight), horizontalSpacing, ref selectedNode);
        }

        private void RemoveNodeButton_Click(object sender, EventArgs e)
        {
            int value;
            if (int.TryParse(RemoveNodeTextBox.Text, out value) == true)
            {
                binaryTree.Remove(value);
            }
            else
            {
                return;
            }

            Draw(ref binaryTree.m_pRoot, Canvas.Location.X + (Canvas.Width / 2), Canvas.Location.Y + (Canvas.Height / rootSpawnPositionHeight), horizontalSpacing, ref selectedNode);
        }
    }
}
