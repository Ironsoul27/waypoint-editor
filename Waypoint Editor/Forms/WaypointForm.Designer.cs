﻿namespace Waypoint_Editor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Canvas = new System.Windows.Forms.PictureBox();
            this.LoadImageButton = new System.Windows.Forms.Button();
            this.AddNodeButton = new System.Windows.Forms.Button();
            this.RemoveNodeButton = new System.Windows.Forms.Button();
            this.LoadProjectButton = new System.Windows.Forms.Button();
            this.SaveProjectButton = new System.Windows.Forms.Button();
            this.ModeSelectText = new System.Windows.Forms.Label();
            this.MoveNodeButton = new System.Windows.Forms.Button();
            this.ClearNodesButton = new System.Windows.Forms.Button();
            this.NodeSizeTextBox = new System.Windows.Forms.TextBox();
            this.NodeSizeLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SelectNodesButton = new System.Windows.Forms.Button();
            this.UnselectNodesButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ConnectAllNodesButton = new System.Windows.Forms.Button();
            this.RemoveConnectionsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).BeginInit();
            this.SuspendLayout();
            // 
            // Canvas
            // 
            this.Canvas.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Canvas.Location = new System.Drawing.Point(12, 12);
            this.Canvas.Name = "Canvas";
            this.Canvas.Size = new System.Drawing.Size(776, 331);
            this.Canvas.TabIndex = 0;
            this.Canvas.TabStop = false;
            this.Canvas.Click += new System.EventHandler(this.Canvas_Click);
            this.Canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseDown);
            this.Canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseMove);
            this.Canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseUp);
            // 
            // LoadImageButton
            // 
            this.LoadImageButton.Location = new System.Drawing.Point(715, 378);
            this.LoadImageButton.Name = "LoadImageButton";
            this.LoadImageButton.Size = new System.Drawing.Size(75, 23);
            this.LoadImageButton.TabIndex = 1;
            this.LoadImageButton.Text = "Load Image";
            this.LoadImageButton.UseVisualStyleBackColor = true;
            this.LoadImageButton.Click += new System.EventHandler(this.LoadImageButton_Click);
            // 
            // AddNodeButton
            // 
            this.AddNodeButton.Location = new System.Drawing.Point(12, 380);
            this.AddNodeButton.Name = "AddNodeButton";
            this.AddNodeButton.Size = new System.Drawing.Size(95, 23);
            this.AddNodeButton.TabIndex = 3;
            this.AddNodeButton.Text = "Add Nodes";
            this.AddNodeButton.UseVisualStyleBackColor = true;
            this.AddNodeButton.Click += new System.EventHandler(this.AddNodeButton_Click);
            // 
            // RemoveNodeButton
            // 
            this.RemoveNodeButton.Location = new System.Drawing.Point(12, 409);
            this.RemoveNodeButton.Name = "RemoveNodeButton";
            this.RemoveNodeButton.Size = new System.Drawing.Size(95, 23);
            this.RemoveNodeButton.TabIndex = 5;
            this.RemoveNodeButton.Text = "Remove Nodes";
            this.RemoveNodeButton.UseVisualStyleBackColor = true;
            this.RemoveNodeButton.Click += new System.EventHandler(this.RemoveNodeButton_Click);
            // 
            // LoadProjectButton
            // 
            this.LoadProjectButton.Location = new System.Drawing.Point(713, 349);
            this.LoadProjectButton.Name = "LoadProjectButton";
            this.LoadProjectButton.Size = new System.Drawing.Size(75, 23);
            this.LoadProjectButton.TabIndex = 6;
            this.LoadProjectButton.Text = "Load Project";
            this.LoadProjectButton.UseVisualStyleBackColor = true;
            this.LoadProjectButton.Click += new System.EventHandler(this.LoadProjectButton_Click);
            // 
            // SaveProjectButton
            // 
            this.SaveProjectButton.Location = new System.Drawing.Point(715, 407);
            this.SaveProjectButton.Name = "SaveProjectButton";
            this.SaveProjectButton.Size = new System.Drawing.Size(77, 23);
            this.SaveProjectButton.TabIndex = 7;
            this.SaveProjectButton.Text = "Save Project";
            this.SaveProjectButton.UseVisualStyleBackColor = true;
            this.SaveProjectButton.Click += new System.EventHandler(this.SaveProjectButton_Click);
            // 
            // ModeSelectText
            // 
            this.ModeSelectText.AutoSize = true;
            this.ModeSelectText.Location = new System.Drawing.Point(12, 330);
            this.ModeSelectText.Name = "ModeSelectText";
            this.ModeSelectText.Size = new System.Drawing.Size(96, 13);
            this.ModeSelectText.TabIndex = 8;
            this.ModeSelectText.Text = "No Mode Selected";
            // 
            // MoveNodeButton
            // 
            this.MoveNodeButton.Location = new System.Drawing.Point(12, 467);
            this.MoveNodeButton.Name = "MoveNodeButton";
            this.MoveNodeButton.Size = new System.Drawing.Size(95, 23);
            this.MoveNodeButton.TabIndex = 9;
            this.MoveNodeButton.Text = "Move Nodes";
            this.MoveNodeButton.UseVisualStyleBackColor = true;
            this.MoveNodeButton.Click += new System.EventHandler(this.MoveNodeButton_Click);
            // 
            // ClearNodesButton
            // 
            this.ClearNodesButton.Location = new System.Drawing.Point(12, 438);
            this.ClearNodesButton.Name = "ClearNodesButton";
            this.ClearNodesButton.Size = new System.Drawing.Size(95, 23);
            this.ClearNodesButton.TabIndex = 11;
            this.ClearNodesButton.Text = "Clear Nodes";
            this.ClearNodesButton.UseVisualStyleBackColor = true;
            this.ClearNodesButton.Click += new System.EventHandler(this.ClearNodesButton_Click);
            // 
            // NodeSizeTextBox
            // 
            this.NodeSizeTextBox.Location = new System.Drawing.Point(335, 365);
            this.NodeSizeTextBox.Name = "NodeSizeTextBox";
            this.NodeSizeTextBox.Size = new System.Drawing.Size(100, 20);
            this.NodeSizeTextBox.TabIndex = 12;
            this.NodeSizeTextBox.Text = "25";
            this.NodeSizeTextBox.TextChanged += new System.EventHandler(this.NodeSizeTextBox_TextChanged);
            // 
            // NodeSizeLabel
            // 
            this.NodeSizeLabel.AutoSize = true;
            this.NodeSizeLabel.Location = new System.Drawing.Point(354, 349);
            this.NodeSizeLabel.Name = "NodeSizeLabel";
            this.NodeSizeLabel.Size = new System.Drawing.Size(56, 13);
            this.NodeSizeLabel.TabIndex = 13;
            this.NodeSizeLabel.Text = "Node Size";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(247, 417);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 16;
            this.label1.Text = "Connections";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(19, 360);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 15);
            this.label2.TabIndex = 17;
            this.label2.Text = "Change Nodes";
            // 
            // SelectNodesButton
            // 
            this.SelectNodesButton.Location = new System.Drawing.Point(113, 438);
            this.SelectNodesButton.Name = "SelectNodesButton";
            this.SelectNodesButton.Size = new System.Drawing.Size(95, 23);
            this.SelectNodesButton.TabIndex = 14;
            this.SelectNodesButton.Text = "Select Nodes";
            this.SelectNodesButton.UseVisualStyleBackColor = true;
            this.SelectNodesButton.Click += new System.EventHandler(this.SelectNodesButton_Click);
            // 
            // UnselectNodesButton
            // 
            this.UnselectNodesButton.Location = new System.Drawing.Point(113, 467);
            this.UnselectNodesButton.Name = "UnselectNodesButton";
            this.UnselectNodesButton.Size = new System.Drawing.Size(95, 23);
            this.UnselectNodesButton.TabIndex = 15;
            this.UnselectNodesButton.Text = "Unselect Nodes";
            this.UnselectNodesButton.UseVisualStyleBackColor = true;
            this.UnselectNodesButton.Click += new System.EventHandler(this.UnselectNodesButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Location = new System.Drawing.Point(123, 417);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 15);
            this.label3.TabIndex = 18;
            this.label3.Text = "Select Nodes";
            // 
            // ConnectAllNodesButton
            // 
            this.ConnectAllNodesButton.Location = new System.Drawing.Point(214, 438);
            this.ConnectAllNodesButton.Name = "ConnectAllNodesButton";
            this.ConnectAllNodesButton.Size = new System.Drawing.Size(135, 23);
            this.ConnectAllNodesButton.TabIndex = 19;
            this.ConnectAllNodesButton.Text = "Connect Selected Nodes";
            this.ConnectAllNodesButton.UseVisualStyleBackColor = true;
            this.ConnectAllNodesButton.Click += new System.EventHandler(this.ConnectAllNodesButton_Click);
            // 
            // RemoveConnectionsButton
            // 
            this.RemoveConnectionsButton.Location = new System.Drawing.Point(214, 467);
            this.RemoveConnectionsButton.Name = "RemoveConnectionsButton";
            this.RemoveConnectionsButton.Size = new System.Drawing.Size(135, 23);
            this.RemoveConnectionsButton.TabIndex = 20;
            this.RemoveConnectionsButton.Text = "Remove Connections";
            this.RemoveConnectionsButton.UseVisualStyleBackColor = true;
            this.RemoveConnectionsButton.Click += new System.EventHandler(this.RemoveConnectionsButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 502);
            this.Controls.Add(this.RemoveConnectionsButton);
            this.Controls.Add(this.ConnectAllNodesButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UnselectNodesButton);
            this.Controls.Add(this.SelectNodesButton);
            this.Controls.Add(this.NodeSizeLabel);
            this.Controls.Add(this.NodeSizeTextBox);
            this.Controls.Add(this.ClearNodesButton);
            this.Controls.Add(this.MoveNodeButton);
            this.Controls.Add(this.ModeSelectText);
            this.Controls.Add(this.SaveProjectButton);
            this.Controls.Add(this.LoadProjectButton);
            this.Controls.Add(this.RemoveNodeButton);
            this.Controls.Add(this.AddNodeButton);
            this.Controls.Add(this.LoadImageButton);
            this.Controls.Add(this.Canvas);
            this.Name = "Form1";
            this.Text = "Waypoint Editor";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Canvas;
        private System.Windows.Forms.Button LoadImageButton;
        private System.Windows.Forms.Button AddNodeButton;
        private System.Windows.Forms.Button RemoveNodeButton;
        private System.Windows.Forms.Button LoadProjectButton;
        private System.Windows.Forms.Button SaveProjectButton;
        private System.Windows.Forms.Label ModeSelectText;
        private System.Windows.Forms.Button MoveNodeButton;
        private System.Windows.Forms.Button ClearNodesButton;
        private System.Windows.Forms.TextBox NodeSizeTextBox;
        private System.Windows.Forms.Label NodeSizeLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button SelectNodesButton;
        private System.Windows.Forms.Button UnselectNodesButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ConnectAllNodesButton;
        private System.Windows.Forms.Button RemoveConnectionsButton;
    }
}

