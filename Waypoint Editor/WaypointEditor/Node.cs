﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.Serialization;
using System.Numerics;

namespace Waypoint_Editor
{
    [Serializable]
    public class Node : ISerializable
    {
        public Image image = null;
        public Vector2 Position { get; set; }
        public Rectangle SelectionCheck { get; set; }
        public Hashtable Connections { get; set; }
        public float Radius { get; set; }
        public string ID { get; set; }


        public Node(ref string id, float radius, ref Vector2 position)
        {         
            SelectionCheck = new Rectangle((int)position.X, (int)position.Y, (int)Radius, (int)Radius);
            Position = new Vector2();
            Connections = new Hashtable();

            this.ID = id;
            this.Radius = radius;
            this.Position = position;
        }

        public Node(SerializationInfo info, StreamingContext context)
        {
            image = (Image)info.GetValue("image", typeof(Image));
            Position = (Vector2)info.GetValue("position", typeof(Vector2));
            Radius = (float)info.GetValue("radius", typeof(float));
            ID = (string)info.GetValue("id", typeof(string));
            Connections = (Hashtable)info.GetValue("connections", typeof(Hashtable));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("image", image, typeof(Image));
            info.AddValue("position", Position, typeof(Vector2));
            info.AddValue("radius", Radius, typeof(float));
            info.AddValue("id", ID, typeof(string));
            info.AddValue("connections", Connections, typeof(Hashtable));           
        }

        public int NodeValue { get; set; }

        public int GetWidth => (image != null) ? image.Width : 0;

        public int GetHeight => (image != null) ? image.Height : 0;  

        public void AddConnection(ref string id, ref Node target)
        {
            Connections.Add(id, target);
        }
        public void RemoveConnection(ref string id)
        {
            Connections.Remove(id);
        }
        public bool IsConnected(ref string id)
        {
            if (Connections.ContainsKey(id))
                return true;

            return false;
        }
    }
}

