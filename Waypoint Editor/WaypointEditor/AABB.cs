﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Waypoint_Editor
{
    public class AABB
    {
        public AABB() { }
        public AABB(ref Vector2 min, ref Vector2 max)
        {
            this.min = min;
            this.max = max;
        }

        public Vector2 dragPoint()
        {
            return new Vector2(max.X, min.Y);
        }

        public Vector2 extents()
        {
            Vector2 temp;
            return temp = new Vector2(Math.Abs(max.X - min.X) * 0.5f,
                                          Math.Abs(max.Y - min.Y) * 0.5f);
        }

        public List<Vector2> corners()
        {
            List<Vector2> corners = new List<Vector2>(4);
            corners[1] = min;
            corners[2] = new Vector2(min.X, max.Y);
            corners[3] = max;
            corners[4] = new Vector2(max.X, min.Y);
            return corners;
        }

        //public Vector2 closestPoint(ref Vector2 p)
        //{
        //    return Vector2.Clamp(p, min, max);
        //}

        public Vector2 min { get; set; }
        public Vector2 max { get; set; }
        
    }
}
