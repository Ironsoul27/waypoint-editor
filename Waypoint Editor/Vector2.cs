﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace Waypoint_Editor
{
    [Serializable]
    public class Vector2 : ISerializable
    {
        public Vector2() { X = Y = 0; }
        public Vector2(ref Vector2 other)
        {
            X = other.X;
            Y = other.Y;
        }
        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static Vector2 operator+ (Vector2 first, Vector2 second)
        {
            return new Vector2(first.X + second.X, first.Y + second.Y);                      
        }

        public float X { get; set; }
        public float Y { get; set; }


        public Vector2(SerializationInfo info, StreamingContext context)
        {
            X = (float)info.GetValue("X", typeof(float));
            Y = (float)info.GetValue("Y", typeof(float));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("X", X, typeof(float));
            info.AddValue("Y", Y, typeof(float));
        }
    }
}
