﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    public class Sort
    {
        private static void Swap(ref List<int> array, int index1, int index2)
        {
            int temp = 0;
            temp = array[index1];
            array[index1] = array[index2];
            array[index2] = temp;
        }

        public static void QuickSort(ref List<int> array, int p, int r)
        {
            int q = 0;

            if (p < r)
            {
                q = Partition(ref array, p, r);
                QuickSort(ref array, p, q - 1);
                QuickSort(ref array, q + 1, r);
            }
        }

        private static int Partition(ref List<int> array, int p, int r)
        {
            int pivot = array[r];
            int i = (p - 1);

            for (int j = p; j <= r - 1; j++)
            {
                if (array[j] <= pivot)
                {
                    i++;
                    Swap(ref array, i, j);
                }
            }
            Swap(ref array, i + 1, r);
            return i + 1;

        }
    }
}