﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace LinkedList
{
    public class LinkedList<T>
    {
        public class Node
        {
            public Node(ref T data, Node next = null, Node previous = null)
            {
                this.data = data;
                this.next = next;
                this.previous = previous;
            }

            public T data;
            public Node next = null;
            public Node previous = null;
        }

        public class Iterator
        {
            public Iterator(Node node = null)
            {
                this.node = node;
            }

            public bool isValid()
            {
                return (node != null);
            }

            public ref T GetData()
            {
                if (node == null)
                {
                    Console.WriteLine("Invalid iterator");
                }
                return ref node.data;
            }

            public bool MoveNext()
            {
                if (node != null)
                {
                    node = node.next;
                }

                return isValid();
            }

            public bool MovePrevious()
            {
                if (node != null)
                {
                    node = node.previous;
                }

                return isValid();
            }

            public Node node = null;
            private const string invalidIterator = "Invalid iterator";
        }

        public LinkedList()
        {
            head = tail = null;
        }

        public bool Empty()
        {
            return (head == null && tail == null);
        }

        public void Clear()
        {
            Node node = head;
            Node nodeToDelete = null;

            while (node != null)
            {
                nodeToDelete = node;
                node = node.next;
                nodeToDelete = null;
            }
            head = tail = null;
            GC.Collect();
        }

        public void PushFront(ref T data)
        {
            Node newNode = new Node(ref data, head);

            if (head != null)
            {
                head.previous = newNode;
            }
            head = newNode;

            if (tail == null)
            {
                tail = newNode;
            }
        }

        public void PushBack(ref T data)
        {
            Node newNode = new Node(ref data, null, tail);

            if (tail != null)
            {
                tail.next = newNode;
            }
            tail = newNode;

            if (head == null)
            {
                head = newNode;
            }
        }

        public void Insert(ref Iterator iter, ref T data)
        {
            if (Empty())
            {
                PushFront(ref data);
            }
            else
            {
                Node insertNode = iter.node;
                if (insertNode != null)
                {
                    Node newNode = new Node(ref data, insertNode.next, insertNode);
                    if (insertNode.next != null)
                    {
                        insertNode.next.previous = newNode;
                    }
                    insertNode.next = newNode;

                    if (tail == insertNode)
                    {
                        tail = newNode;
                    }
                }
                else
                {
                    Console.WriteLine(emptyList);
                }
            }
        }

        public void PopFront()
        {
            if (head == null)
            {
                Console.WriteLine(emptyList);
            }
            else
            {
                Node node = head;

                if (head == tail)
                {
                    head = tail = null;
                }
                else
                {
                    head = head.next;
                    head.previous = null;
                }

                node = null;

                GC.Collect();
            }
        }

        public void PopBack()
        {
            if (head == null)
            {
                Console.WriteLine(emptyList);
            }
            else
            {
                Node node = head;

                if (head == tail)
                {
                    head = tail = null;
                }
                else
                {
                    head = head.previous;
                    head.next = null;
                }

                node = null;

                GC.Collect();
            }
        }

        public void Erase(ref Iterator iter)
        {
            if (Empty())
            {
                Console.WriteLine(emptyList);
            }
            else
            {
                if (tail != head)
                {
                    if (iter.node == head)
                    {
                        head = iter.node.next;
                        iter.node.next.previous = null;
                    }
                    if (iter.node == tail)
                    {
                        tail = iter.node.previous;
                        iter.node.previous.next = null;
                    }
                    if (iter.node.next != null)
                        iter.node.next.previous = iter.node.previous;
                    if (iter.node.previous != null)
                        iter.node.previous.next = iter.node.next;
                }
                else
                {
                    head = tail = null;
                }

                iter.node = null;

                GC.Collect();                 
            }
        }

        public void Remove(ref T data)
        {
            uint deletedElements = 0;
            Iterator it = new Iterator(head);
            Node node = it.node;

            while(it.node != null)
            {
                if (it.node.data.Equals(data))
                {
                    node = it.node.next;
                    Erase(ref it);
                    deletedElements++;
                    it = new Iterator(node);
                }
                else
                {
                    if (!it.MoveNext())
                    {
                        break;
                    }
                }
            }
            if (head == null && deletedElements == 0)
                Console.WriteLine("Array is empty\n");
            else if (deletedElements != 0)
                Console.WriteLine("Deleted: " + deletedElements + " nodes of element: " + data);
            else
                Console.WriteLine("Could not find any " + data + "'s in the list\n");
        }

        public ref T First()
        {
            if (head == null)
            {
                Debug.Assert(head == null);
            }
            return ref head.data;
        }

        public ref T Last()
        {
            if (tail == null)
            {
                Debug.Assert(tail == null);
            }
            return ref tail.data;
        }

        public bool SearchFor(T data)
        {
            List<T> list = new List<T>();
            Node node = head;
            while (node != null)
            {
                list.Add(node.data);
                node = node.next;
            }

            if (Search<T>.BinarySearch(ref list, 0, list.Count() - 1, data).Equals(data))
            {
                return true;
            }
            return false;                       
        }

        public Node Begin()
        {
            return head;
        }

        //public Iterator Begin()
        //{
        //    return Iterator(head);
        //}

        //public Iterator End()
        //{
        //    return Iterator(tail);
        //}


        public uint Count()
        {
            uint counter = 0;
            Node node = head;

                while (node != null)
                {
                    counter++;
                    node = node.next;
                }

            return counter;
        }

        public void Print()
        {
            Node node = head;

            while (node != null)
            {
                Console.WriteLine(node.data);
                node = node.next;
            }
        }       


        private Node head = null;
        private Node tail = null;
        private const string emptyList = "Array is empty";
    }
}
