﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            LinkedList<int> LL = new LinkedList<int>();
            int i = 5;
            LL.PushBack(ref i);
            LL.Remove(ref i);
            Console.WriteLine(LL.Count());
            LL.Print();
            Stopwatch sw = new Stopwatch();


            //Timer timer = new Timer();
            
            for (int j = 0; j <= 100000; j++)
            {
                int k = random.Next(50);
                LL.PushBack(ref k);
            }

            int m = 9;
            LL.Remove(ref m);

            if (LL.SearchFor(m))
                Console.WriteLine("Found the number");

            List<int> list = new List<int>(100000);
            LinkedList<int>.Iterator it = new LinkedList<int>.Iterator(LL.Begin());

            for (int p = 0; p < LL.Count(); p++)
            {
                list.Add(it.GetData());
                it.MoveNext();
            }

            sw.Start();

            Sort.QuickSort(ref list, 0, (int)LL.Count() - 1);
           
            sw.Stop();
            Console.WriteLine("Sort took: " + sw.ElapsedMilliseconds / 1000.0f + " Seconds");
            
            //for (int r = 0; r < list.Count(); r++)
            //{
            //    Console.WriteLine(list[r]);
            //}
            Console.ReadLine();
        }
    }
}
