﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    public class Search<T>
    {
        private static bool IsLesserThan(T key, ref List<T> data, int pivot)
        {
            if (System.Collections.Generic.Comparer<T>.Default.Compare(key, data[pivot]) < 0)
                return true;

            return false;
        }

        public static int BinarySearch(ref List<T> data, int start_index, int end_index, T key)
        {
            while (start_index <= end_index)
            {
                int pivot = (start_index + end_index) / 2;

                if (data[pivot].Equals(key))
                    return pivot;                                       
                
                if (IsLesserThan(key, ref data, pivot))
                    end_index = pivot - 1;

                else
                    start_index = pivot + 1;
            }
            Console.WriteLine("Number not found\n");
            //int? value = null;
            return -1;
        }
    }
}











//public static dynamic Convet(dynamic source, Type dest)
//{
//    return Convert.ChangeType(source, dest);
//}

//public static T Cast<T>(object o)
//{
//    return (T)o;
//}